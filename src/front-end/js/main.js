var counter = function() {
    return {
        _countTime: 0,
        _callback: function() {

        },

        setMilliseconds(timeMS, callback) {
            this._countTime = timeMS;
            this._callback = callback;
        },

        setSeconds(timeS, callback) {
            this._countTime = timeS * 1000;
            this._callback = callback;
        },

        start() {
            setTimeout(this._callback, this._countTime);
        }
    };
};
//
// watch
//
// var = function() {
//
// }