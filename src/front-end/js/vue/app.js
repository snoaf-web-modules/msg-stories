Vue.config.productionTip = false;

var vm = new Vue({
    el: '.app',
    data: {
        story: story,
        script: this.story.script,
        status: localStorage.getObject('status') || {decisions: {}, progress: 0},
        currentLocation: window.location.pathname,
        test: 'hello',
        pauseTimeS: 1,
        showMenu: false
    },
    computed: {
        scriptLocation() {
            return this.status.progress || 0;
        },
        optionList() {
            return this.script[this.scriptLocation] &&
                this.script[this.scriptLocation].options || [];
        }
    },
    created() {
        this.play();
    },
    methods: {
        play() {
            var self = this;
            var positionOnCall = this.status.progress;

            setTimeout(function() {
                // Checking that the position on call is the same assures
                // that the timer isn't going off after a reset
                if (positionOnCall == self.status.progress &&
                    self.incrementScriptLocation())
                {
                    self.play();
                }
            }, this.pauseTimeS * 1000);
        },
        toggleMenu(event) {
            if (typeof event == 'boolean')
            {
                this.showMenu = event;
            }
            else
            {
                this.showMenu = !this.showMenu;
            }
            console.log('showMenu is now', this.showMenu)
        },
        reset() {
            this.status = {decisions: {}, progress: 0};
            this.play();
        },
        getDecision(messageId) {
            return this.status.decisions &&
                this.status.decisions[messageId];
        },
        waitingForInput() {
            setTimeout(this.scrollToBottom, 0.5);
            return (
                this.optionList.length > 0 &&
                this.getDecision(this.scriptLocation) == null
            );
        },
        scrollToBottom() {
            var objDiv = document.getElementsByClassName("conversation")[0];
            objDiv.scrollTop = objDiv.scrollHeight;
        },
        selectOption(option) {
            this.status.decisions[this.scriptLocation] = option;

            this.save();
            this.incrementScriptLocation();
            this.play()
        },
        incrementScriptLocation() {
            if (!this.waitingForInput() && this.status.progress < this.script.length) {
                this.status.progress++;

                this.save();
                setTimeout(this.scrollToBottom, 0);

                return true;
            }
            return false;
        },
        save() {

        }
    }
});

