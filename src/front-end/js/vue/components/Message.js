var messageHtml =
    `<div :class="classes">
        <div class="bubble">
            <p v-html="message.text"></p>
        </div>
    </div>`;

Vue.component('Message', {
    props: ['message'],
    template: messageHtml,
    computed: {
        classes: function() {
            let classes = ["bubble-holder", "row"];
            if (this.message.sender == "self")
            {
                classes.push("sent");
                classes.push("align-self-end");
            }
            return classes;
        }
    }
});