var chatOptionsHtml =
    `<div class="bubble-holder options" :class="{hide: hidden}">
        <div v-for="(option, key) in options" class="bubble" @click="chooseOption(key)">
            <p v-html="option.prompt + 'hello' + key"></p>
        </div>
    </div>`;

Vue.component('ChatOptions', {
    props: ['chooseOption', 'options'],
    template: chatOptionsHtml,
    computed: {
        hidden: function () {
            return this.options.length == 0;
        }
    }
});