var conversationHtml =
    `<div class="conversation" @click="check()">
        <message v-for="(message, key) in script" v-if="key <= status.progress" :message="message" key="key"></message>
    </div>`;

Vue.component('Conversation', {
    props: {
        script: Array,
        status: Object
    },
    template: conversationHtml,
    methods: {
        check: function() {
            console.log('In the convo', this.status.progress);
        }
    }
});