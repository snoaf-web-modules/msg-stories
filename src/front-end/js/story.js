var story = {
    contacts: {
        contact1: {
            name: 'Contact 1',
            imageFile: '',
            startingContact: true
        }
    },
    conversations: [
        {
            conversationId: 0,
            contacts: ['contact1'],
            name: null
        }
    ],
    script: [
        {
            conversationId: 0,
            text: 'Hi there.',
            sender: 'contact1'
        },
        {
            conversationId: 0,
            text: 'The second line.',
            sender: 'self'
        },
        {
            conversationId: 0,
            text: `Hello, this here is a pretty long sentence<br><br>And here is a new line`,
            sender: 'contact1'
        },
        {
            conversationId: 0,
            text: 'hi<br>this also has multiple lines',
            sender: 'contact1'
        },
        {
            conversationId: 0,
            sender: 'self',
            options: [
                {
                    prompt: 'Option 1',
                    text: 'This is option 1; it has been picked'
                },
                {
                    prompt: 'Option 2',
                    text: 'This is option 2; it has been picked'
                }
            ]
        },
        {
            conversationId: 0,
            text: 'This comes after the question',
            sender: 'contact1'
        }
    ]
};