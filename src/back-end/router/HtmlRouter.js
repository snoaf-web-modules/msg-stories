'use strict';

let core = require('core-libs');
let bootstrap   = core.bootstrap;
let hitch       = core.functions.hitch;
let CoreRouter      = core.router;

let StoriesRenderer = require('../renderer/StoriesRenderer');

class HtmlRouter extends CoreRouter {
    init() {
        return super.init('msg-stories')
            .then(() => {
                this.start();
                return Promise.resolve();
            });
    }

    start() {
        // Install routes
        this.app
            .get('/*', hitch(StoriesRenderer, 'getPage'))
        ;

    }
}

let _singleton = new HtmlRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;