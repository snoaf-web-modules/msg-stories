'use strict';

let merge = require('deepmerge');

let core = require('core-libs');
let CoreContext = core.context;

class StoriesRenderContextFactory {

    constructor() {}

    init(req) {
        let selectorCtx = new StoriesRenderContextMixin(req);
        let merged = merge(req.ctx, selectorCtx);

        req.ctx = merged;
        return merged;
    }
}

class StoriesRenderContextMixin extends CoreContext {

    constructor(req) {
        super(req);
    }
}

module.exports.factory = new StoriesRenderContextFactory();
module.exports.mixin = StoriesRenderContextMixin;