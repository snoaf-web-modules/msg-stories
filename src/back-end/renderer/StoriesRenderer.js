'use strict';

let path = require('path');
let core = require('core-libs');

let bootstrap   = core.bootstrap;
let renderEngine    = core.renderEngine;

let StoriesRenderCtx = require('./context/StoriesRenderContext').factory;

class StoriesRenderer {

    init() {
        this.pages = {
            '/': path.join('chat.html')
        };

        return Promise.resolve();
    }

    getPage(req, res, next) {
        if (this.pages.hasOwnProperty(req.url))
        {
            let filePath = renderEngine.getPagePath('msg-stories', this.pages[req.url]);
            res.render(filePath, StoriesRenderCtx.init(req));
        }
        else
        {
            next();
        }
    }
}

let _singleton = new StoriesRenderer();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;